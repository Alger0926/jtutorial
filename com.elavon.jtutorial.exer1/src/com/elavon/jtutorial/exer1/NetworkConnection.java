package com.elavon.jtutorial.exer1;

public interface NetworkConnection {
	public NetworkConnection connect(String name);
	public boolean connectionStatus();
}
