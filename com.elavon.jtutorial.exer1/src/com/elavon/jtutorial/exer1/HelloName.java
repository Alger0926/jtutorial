package com.elavon.jtutorial.exer1;

public class HelloName {
	public static void main(String[] args) {
		System.out.println("   ***    **        ******   ******** ********   ******   *******  **    ** ");
		System.out.println("  ** **   **       **    **  **       **     ** **    ** **     ** ***   ** ");
		System.out.println(" **   **  **       **        **       **     ** **       **     ** ****  ** ");
		System.out.println("**     ** **       **   **** ******   ********   ******  **     ** ** ** ** ");
		System.out.println("********* **       **    **  **       **   **         ** **     ** **  **** ");
		System.out.println("**     ** **       **    **  **       **    **  **    ** **     ** **   *** ");
		System.out.println("**     ** ********  ******   ******** **     **  ******   *******  **    ** ");
		System.out.println("");
		System.out.println("**       **     **  ******  ******** ********   *******   ");
		System.out.println("**       **     ** **    ** **       **     ** **     **  ");
		System.out.println("**       **     ** **       **       **     ** **     **  ");
		System.out.println("**       **     ** **       ******   ********  **     **  ");
		System.out.println("**       **     ** **       **       **   **   **     **  ");
		System.out.println("**       **     ** **    ** **       **    **  **     **  ");
		System.out.println("********  *******   ******  ******** **     **  *******   ");
		System.out.println("");                                                              
		System.out.println("**     **    ***    **    **    ***    **        *******  ");
		System.out.println("***   ***   ** **   ***   **   ** **   **       **     ** ");
		System.out.println("**** ****  **   **  ****  **  **   **  **       **     ** ");
		System.out.println("** *** ** **     ** ** ** ** **     ** **       **     ** ");
		System.out.println("**     ** ********* **  **** ********* **       **     ** ");
		System.out.println("**     ** **     ** **   *** **     ** **       **     ** ");
		System.out.println("**     ** **     ** **    ** **     ** ********  *******  ");

	}
}
