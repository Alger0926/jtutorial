package com.elavon.tutorial.xmas;

public class TwelveDaysOfChristmas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TwelveDaysOfChristmas().printLyrics();
	}
	
	String []gifts;
	String []intro;
	
	TwelveDaysOfChristmas () {
		gifts = new String[12];
		gifts[0] = "a Partridge in a Pear Tree";
		gifts[1] = "Two Turtle Doves";
		gifts[2] = "Three French Hens";
		gifts[3] = "Four Calling Birds";
		gifts[4] = "Five Golden Rings";
		gifts[5] = "Six Geese a Laying";
		gifts[6] = "Seven Swans a Swimming";
		gifts[7] = "Eight Maids a Milking";
		gifts[8] = "Nine Ladies Dancing";
		gifts[9] = "Ten Lords a Leaping";
		gifts[10] = "Eleven Pipers Piping";
		gifts[11] = "12 Drummers Drumming";
		
		intro = new String[12];
		intro[0] = "First";
		intro[1] = "Second";
		intro[2] = "Third";
		intro[3] = "Fourth";
		intro[4] = "Fifth";
		intro[5] = "Sixth";
		intro[6] = "Seventh";
		intro[7] = "Eighth";
		intro[8] = "Nineth";
		intro[9] = "Tenth";
		intro[10] = "Eleventh";
		intro[11] = "12th";
	}
	
	public void printIntro(int x) {
		System.out.println("On the " + intro[x] + " day of Christmas my true love sent to me");
	}
	
	public void printGifts(int x) {
		if (x == 0) {
			System.out.println(gifts[x]);
		}
		else {
			for(int i = x; i >= 0; i--) {
				if (i == 0) {
					System.out.println("and " + gifts[i]);
				}
				else {
					System.out.println(gifts[i]);
				}
			}
		}
	}
	
	public void printLyrics() {
		for(int i = 0; i < 12; i++) {
			printIntro(i);
			printGifts(i);
			System.out.println("");
		}
	}

}
