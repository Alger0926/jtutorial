package com.elavon.training.calculatorimpl;
import com.elavon.training.calculator.*;

public class AwesomeCalculatorImpl implements AwesomeCalculator {

	@Override
	public int getSum(int augend, int addend) {
		// TODO Auto-generated method stub
		return(augend + addend);
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		// TODO Auto-generated method stub
		return(minuend - subtrahend);
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		// TODO Auto-generated method stub
		return(multiplicand + multiplier);
	}

	@Override
	//NOTE: dividend and divisor must be int so that the function to get remainder is possible
	public String getQuotientAndRemainder(double dividend, double divisor) {
		// TODO Auto-generated method stub 
		int qoutient = (int) (dividend) / (int)(divisor);
		int remainder = (int) (dividend) % (int)(divisor);
		return(qoutient + " remainder " + remainder);
	}

	@Override
	public double toCelsius(int fahrenheit) {
		// TODO Auto-generated method stub
		return((fahrenheit - 32.00)*5/9);
	}

	@Override
	public double toFahrenheit(int celsius) {
		// TODO Auto-generated method stub
		return(celsius * 9/5 + 32.00);
	}

	@Override
	public double toKilogram(double lbs) {
		// TODO Auto-generated method stub
		return(lbs / 2.20462262185);
	}

	@Override
	public double toPound(double kg) {
		return(kg * 2.20462262185);
	}

	@Override
	public boolean isPalindrome(String str) {
		String sp = str.toLowerCase();
		
		int length = sp.length();
		boolean palindrome = false;
		for(int i = 0; i < (length/2); i++) {
			if(sp.charAt(i) == sp.charAt(length - i - 1)) {
				palindrome = true;
			}
			else {
				break;
			}
		}
		return(palindrome);
	}

	@Override
	public int getSum(int... summands) {
		int sum = 0;
		for(int x : summands) {
			sum += x;
		}
		return(sum);
	}

	@Override
	public double getProduct(double... factors) {
		double product = 1;
		for(double x : factors) {
			product *= x;
		}
		return(product);
	}
}
