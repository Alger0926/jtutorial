package com.elavon.training;

import com.elavon.training.calculatorimpl.AwesomeCalculatorImpl;

public class Main extends AwesomeCalculatorImpl {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AwesomeCalculatorImpl m = new AwesomeCalculatorImpl();
		System.out.println("1 + 3 = " + m.getSum(1, 3));
		System.out.println("100 - 99 = " + m.getDifference(100, 99));
		System.out.println("11 * 9 = " + m.getProduct(11, 9));
		System.out.println("7 / 5 = " + m.getQuotientAndRemainder(7, 5));
		System.out.println("100 Farenheit is equal to " + m.toCelsius(100) + " Celcius");
		System.out.println("37 Celcius is equal to " + m.toFahrenheit(37) + " Farenheit");
		System.out.println("10 Kgs is equal to " + m.toPound(10) + " lbs");
		System.out.println("100 Lbs is equal to " + m.toKilogram(100) + " kgs");
		System.out.println("Hannah is a palindrome?" + m.isPalindrome("Hannah"));
		System.out.println("Montana is a palindrome?" + m.isPalindrome("Montana"));
		System.out.println("aSA is a palindrome?" + m.isPalindrome("aSA"));
		System.out.println("Sum of 1, 2, 3, 4 = " + m.getSum(1, 2, 3, 4));
		System.out.println("Product of 1, 2, 3, 4 = " + m.getProduct(1, 2, 3, 4));
	}

}
